//Вызов формы
let form = document.getElementById("form");
let btn = document.getElementsByClassName("contact__button")[0];
let btn1 = document.getElementsByClassName("contact__button")[1];
let close = document.getElementById("close");


$(document).ready(function() {
	//Открытие формы(кнопка 1)
	$("#c").click(function () {
        //Отображаем меню
        $("body").toggleClass("open-nav");
        $("#form").css("display", "flex");
        $("#forma").css("display", "flex");
        $(".contact__button").css("background-color", "#e6e9ee");
		$(".contact__button").css("color", "#a7b0c1");
        // Анимируем увеличение ширины
        $("#form").animate({
            width: "100%"
        });
    });
    //Открытие формы(кнопка 2)
    $("#cont").click(function () {
        //Отображаем меню
        $("body").toggleClass("open-nav");
        $("#form").css("display", "flex");
        $("#forma").css("display", "flex");
        $(".contact__button").css("background-color", "#e6e9ee");
		$(".contact__button").css("color", "#a7b0c1");
        $("#form").animate({
            width: "100%"
        });
    });
    //Закрытие формы
    $("#close").click(function () {
    	//Скрываем меню
        $("#forma").css("display", "none");
        $(".contact__button").css("background-color", "#24aa17");
		$(".contact__button").css("color", "#ffffff");
        $("#form").animate({
            width: "0%"
        }, {
            complete: function () {
                $("body").toggleClass("open-nav");
            }
        });
    });

    //Отображение модального окна
    var showModal = function () {
        //Просто добавляем или убираем класс display: none;
        $("#form").toggleClass("display-none");
        let modal = document.getElementById("#form");
        //Запускаем анимации
        animateModal({
            duration: 300,
            timing: function (timeFraction) {
                return timeFraction;
            },
            //Отрисовка анимации
            draw: function (progress) {
                //Прозрачность окна равна прогрессу анимации (от 0 до 1)
                modal.style.opacity = progress;
                if ($(document).width() > 767) {
                    //Отступ сверху
                    modal.style.marginTop = progress * 4 + "rem";
                }
            }
        });
    }
    //Анимация модального окна
    function animateModal({
        duration,
        timing,
        draw
    }) {
        //Начало анимации
        let start = performance.now();
        //Анимация с помощью requestAnimationFrame
        requestAnimationFrame(function animate(time) {
            //Время анимации
            let timeFraction = (time - start) / duration;
            if (timeFraction > 1) timeFraction = 1;

            let progress = timing(timeFraction)
            //Отрисовка
            draw(progress);
            //Запускаем анимацию заново, если она ещё не закончилась
            if (timeFraction < 1) {
                requestAnimationFrame(animate);
            }
        });
    }

    //History API
    btn.onclick = function() {
		history.pushState({page: 2}, "title 2", "?Form");
	}
	btn1.onclick = function() {
		history.pushState({page: 2}, "title 2", "?Form");
	}
	close.onclick = function() {
		history.pushState({page: 2}, "title 2", "?Home");
	}
	addEventListener("popstate",function(e){
	    form.style.display = "none";
	    btn.style.backgroundColor = "#24aa17";
	    btn.style.color = "#ffffff";
	    history.pushState({page: 1}, "title 1", "?Home");
	}, true);

	//LocalStorage
	if (window.localStorage) {
		//Сохраняем данные input, textarea
		var elements = document.querySelectorAll('[name]');
		for (var i = 0, length = elements.length; i < length; i++) {
			(function(element) {
				var name = element.getAttribute('name');
				element.value = localStorage.getItem(name) || '';
				element.onkeyup = function() {
					localStorage.setItem(name, element.value);
				};
			})(elements[i]);
		}
	}
})