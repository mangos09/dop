class Form extends React.Component {
	constructor(props) {
		//Наследуем родителя
		super(props);

		var name = localStorage.getItem("name") || "";
		var nameIsValid = this.validateName(name);
		var phone = localStorage.getItem("phone") || "";
        // Берем значение комментария из LocalStorage
        var comment =  localStorage.getItem("comment") || "";

        this.state = {
            name: name,
            phone: phone,
            comment: comment,
            nameValid: nameIsValid
        }

        //Биндинг всех методов
        this.changeName = this.changeName.bind(this);
        this.changePhone = this.changePhone.bind(this);
        this.changeComment = this.changeComment.bind(this);
        this.submit = this.submit.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.blocked = this.blocked.bind(this);
        this.unblocked = this.unblocked.bind(this);
	}

	//Блокировка и разблокировка кнопки "Отпрваить"
	blocked(btn) {
		btn.disabled = true;
	}
	unblocked(btn) {
		btn.disabled = false;
	}

	validateName(name) {
        return name.length > 0;
    }

    //Изменение имени
    changeName(event) {
        //Берем значение инпута имени
        let val = event.target.value;
        console.log("Имя: " + val);
        //Проверяем валидность имени
        var valid = this.validateName(val);
        //Меняем состояние
        this.setState({
            name: event.target.value,
            nameValid: valid
        });
        //Записываем в LocalStorage
        localStorage.setItem("name", event.target.value);
    }

    //Изменение номера телефона
    changePhone(event) {
        //Берем значение инпута почты
        var val = event.target.value;
        console.log("Телефон: " + val);
        //Меняем состояние
        this.setState({
            mail: event.target.value
        });
        //Записываем в LocalStorage
        localStorage.setItem("mail", event.target.value);
    }

    //Изменение комментария
    changeComment(event) {
        // Берем значение поля комментария
        var val = event.target.value;
        // Выводим в консоль введенные символы
        console.log("Comment: " + val);
        // Меняем состояние
        this.setState({
            comment: event.target.value
        });
        // Записываем в LocalStorage
        localStorage.setItem("comment", event.target.value);
    }

    //Очищаем localStorage
    resetForm() {
        // Меняем поля на пустые строки
        localStorage.setItem("name", "");
        localStorage.setItem("phone", "");
        localStorage.setItem("comment", "");
        // Меняем состояние
        this.setState({
            name: "",
            phone: "",
            comment: "",
        });
        // Очищаем инпуты и поля
        $(".name1").value = "";
        // Закрываем форму с помощью кнопки назад
        history.back();
    }

    //Обработчик нажатия кнопки назад
    handleSubmit(e) {
        //Запрет обновления страницы
        e.preventDefault();
        //Проверка валидности имени
        if (this.state.nameValid === true) {
            //Включаем блокировку кнопки
            this.blocked(document.getElementById("button-submit"));
            //Отправляем форму на сервер
            fetch("https://formcarry.com/s/VYPDe1IxTiA", {
                method: "POST",
                body: JSON.stringify(this.state),
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
            }).then(
                (response) => (response.json())
            ).then((response) => {
                //Если успешно
                if (response.status === "success") {
                    //Выводим сообщение об отправке
                    alert("Сообщение отправлено!");
                    //Убираем блокировку кнопки
                    this.unblocked(document.getElementById("button-submit"));
                    //Очищаем и закрываем форму
                    this.resetForm();
                //Если ошибка
                } else if (response.status === "fail") {
                    //Выводим сообщение об ошибке
                    alert("Ошибка!!!! отправки! Пожалуйста, повторите попытку.")
                    //Отключаем бллокировку кнопки
                    this.hide(document.getElementById("button-submit"));
                }
            })
        } else {
            alert("Ошибка ввода! Проверьте корректность введеного имени.");
        }
    }
    render() {
    	return (
    		<div className="reactForm">
    			
    		</div>
    	)
    }

}